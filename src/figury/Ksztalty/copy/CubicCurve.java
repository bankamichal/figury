package figury.Ksztalty.copy;

import figury.Figura;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.CubicCurve2D;

/**
 * Created by MichalPC on 2017-12-13, 19:52.
 */
public class CubicCurve extends Figura {
    public CubicCurve(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        shape = new CubicCurve2D.Float(0,0,0,30,20,0,20,10);
        aft = new AffineTransform();
        area = new Area(shape);
    }
}

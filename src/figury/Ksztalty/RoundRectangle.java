package figury.Ksztalty;

import figury.Figura;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

/**
 * Created by MichalPC on 2017-12-13, 19:42.
 */
public class RoundRectangle extends Figura{
    public RoundRectangle(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);
        shape = new RoundRectangle2D.Float(0,0,20,20,4,4);
        aft = new AffineTransform();
        area = new Area(shape);
    }
}

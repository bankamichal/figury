package figury;

import figury.Ksztalty.CubicCurve;
import figury.Ksztalty.Elipsa;
import figury.Ksztalty.Kwadrat;
import figury.Ksztalty.RoundRectangle;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	Graphics2D buffer;

	List<Thread> threads = new ArrayList<>();
    Image image2 = null;
    Graphics2D device2 = null;
    Graphics2D buffer2 = null;
	
	private int delay = 20;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		this.setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void clear()
	{
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		threads = new ArrayList<>();

		delay = 20;

		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	void addFig() {
		Figura fig = null;
		final int numberOfFigures = 4;
		if (numer % numberOfFigures == 0)
		{
			fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		}
		else if (numer % numberOfFigures == 1)
		{
			fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		}
		else if (numer % numberOfFigures == 2)
		{
			fig = new RoundRectangle(buffer,delay,getWidth(),getHeight());
		}
		else if (numer % numberOfFigures == 3)
		{
			fig = new CubicCurve(buffer,delay,getWidth(),getHeight());
		}
		numer++;
		timer.addActionListener(fig);
		Thread f = new Thread(fig);
		f.start();
		threads.add(f);
	}

	void animate()
    {
		if (timer.isRunning())
        {
        	/*synchronized (timer)
			{
				try {
					System.out.println("WAIT");
					timer.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}*/
			timer.stop();
		}
		else
        {
        	/*synchronized (timer)
			{
				timer.notifyAll();
			}*/

			timer.start();
		}
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}

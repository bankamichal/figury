/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private int width;
	private int height;
	private Color clr;

	private int r;
	private int g;
	private int b;
	boolean rUp = true;
	boolean gUp = true;
	boolean bUp = true;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();

		r = rand.nextInt(255);
		g = rand.nextInt(255);
		b = rand.nextInt(255);
		clr = new Color(r, g, b);
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(100, 100);
		area.transform(aft);
		shape = area;

		while (true) {
			// przygotowanie nastepnego kadru
			shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	protected Shape nextFrame() {
			// zapamietanie na zmiennej tymczasowej
			// aby nie przeszkadzalo w wykreslaniu
			area = new Area(area);
			aft = new AffineTransform();
			Rectangle bounds = area.getBounds();
			int cx = bounds.x + bounds.width / 2;
			int cy = bounds.y + bounds.height / 2;
			// odbicie
			if (cx < 70 || cx > width - 70)
				dx = -dx;
			if (cy < 70 || cy > height-70)
				dy = -dy;
			// zwiekszenie lub zmniejszenie
			if (bounds.height > height / 5 || bounds.height < 10)
				sf = 1 / sf;
			// konstrukcja przeksztalcenia



		if (r == 255)
		{
			rUp = false;
		}
		else if (r == 0)
		{
			rUp = true;
		}
 //////////////////////////////
		if (g == 255)
		{
			gUp = false;
		}
		else if (g == 0)
		{
			gUp = true;
		}
/////////////////////////////
		if (b == 255)
		{
			bUp = false;
		}
		else if (b == 0)
		{
			bUp = true;
		}
//////////////////////////////
		if (rUp) r++;
		else r--;

		if (gUp) g++;
		else g--;

		if (bUp) b++;
		else b--;


		clr = new Color(r,g,b);
			aft.translate(cx, cy);
			aft.scale(sf, sf);
			aft.rotate(an);
			aft.translate(-cx, -cy);
			aft.translate(dx, dy);
			// przeksztalcenie obiektu
			area.transform(aft);

		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}

}

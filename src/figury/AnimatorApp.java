package figury;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class AnimatorApp extends JFrame {

	int figuresNum = 0;
	Rectangle r = getBounds();
	public int winWidth = (int) (r.getWidth() - 100);
	public int winHeight = (int) (r.getHeight() - 100);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.setVisible(true);
					frame.setBackground(Color.WHITE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		System.out.println(screen.getWidth() + "  " + screen.getHeight());
		int ww = 1000, wh = 700;
		setBounds(200, 200,ww,wh);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//setExtendedState(JFrame.MAXIMIZED_BOTH);

		AnimPanel kanwa = new AnimPanel();

		kanwa.setBounds(50,50,ww -50,wh - 50);
		contentPane.add(kanwa);

		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
				figuresNum++;
			}
		});
		btnAdd.setBounds(10, 10, 80, 23);
		contentPane.add(btnAdd);


		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}

		});
		btnAnimate.setBounds(100, 10, 120, 23);
		contentPane.add(btnAnimate);

		addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
				kanwa.setBounds(50,50,(int)r.getWidth()-100,(int)r.getHeight()-100);
			}

			@Override
			public void componentMoved(ComponentEvent e) {}
			@Override
			public void componentShown(ComponentEvent e) {}
			@Override
			public void componentHidden(ComponentEvent e) {}
		});
		
	}

}
